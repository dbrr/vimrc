set nocompatible

no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>

ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>

" FINDING FILES
" Search down into subfolders
" Provides tab-completeion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" ALLLOWS:
"  - Hit tab to :find by partial match
"  - Use * to make it fuzzy



" TAG JUMPING:



